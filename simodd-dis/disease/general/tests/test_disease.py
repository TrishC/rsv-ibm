from random import Random
from unittest import TestCase

from population.pop_hh import PopHH
from disease.general.ind_epi import IndEpi
from disease.general.contact_matrix import ContactMatrix

__author__ = 'ngeard'

class TestDiseaseExposure(TestCase):
    def setUp(self):
        self.P = PopHH(IndEpi)
        self.P.construct_hh_age_pop([[23, 22, 8, 7]])
        self.P.construct_age_pop(100, [1/100] * 100)
        self.cm = ContactMatrix()
        self.cm.init_age_classes(self.P)
        self.cm.init_contact_matrix(1)
        self.cm.export_contact_matrix('test_cm.csv')

