from collections import defaultdict

from disease.general.vaccine import VaccineSingle

__author__ = 'ngeard'


class VaccineAntenatal(VaccineSingle):
    def __init__(self, label, age, age_days, coverage, t_per_year, offset, rng,
                 precond=None, fail_prob=0.0, v_state='R', ga_min=42, ga_max=90):
        super(VaccineAntenatal, self).__init__(label, age, age_days, coverage, t_per_year, offset, rng,
                                               precond, fail_prob, v_state)
        self.ga_min = ga_min
        self.ga_max = ga_max

        # dictionary keyed by time, of mothers eligible for vaccination at that time
        self.vacc_schedule = defaultdict(list)
        # dictionary mapping mothers to the scheduled vaccination time
        self.scheduled = {}

    def get_candidates(self, t, P):
        """
        Select candidates for antenatal vaccination on the basis of currently being pregnant.

        Will need to refine to target based on timing -- preg current is a dictionary mapping
        mother to time due, so ought be calculable on the basis of dictionary values and t (current time).

        :param t: the current time
        :param P: the population to vaccinate from
        :return: a list of candidates eligible for vaccination
        """

        # pregnant women eligible on basis of child's gestational age:
        # minimum and maximum gestational age (ga) for eligibility (in days)
        # ga_min = 42 # ie, 6 weeks
        # ga_max = 90 # ie, 3 months

        for x in P.preg_current:
            # skip if already scheduled (shouldn't be necessary, but just in case)
            if x in self.scheduled and not self.scheduled[x] < t:
                # print("cur time", t, "vacc sched", self.scheduled[x], "due date",
                # P.preg_current[x], "kids", len(x.children))
                continue
            # schedule vaccine time if a newly pregnant mother
            if P.preg_current[x] - t == int(280.0 / (364.0 / self.t_per_year)):
                # sample vaccination time uniformly in range (t + ga_min, t + ga_max)
                vacc_time = t + int(280.0 / (364.0 / self.t_per_year)) - (
                    self.rng.randint(int(self.ga_min / (364 / self.t_per_year)),
                                     int(self.ga_max / (364 / self.t_per_year))))
                # add this mother to the 'scheduled' list
                self.scheduled[x] = vacc_time
                # add to the vaccine schedule
                self.vacc_schedule[vacc_time].append(x)

        # get list of mothers eligible for vaccination this time step
        candidates = self.vacc_schedule[t]

        del self.vacc_schedule[t]

        # convert ga to due dates
        # due_min = t + int((280 - self.ga_max) / (364.0 / self.t_per_year))
        # due_max = t + int((280 - self.ga_min) / (364.0 / self.t_per_year))

        # candidates = [x for x in P.preg_current
        #               if due_min <= P.preg_current[x] <= due_max
        #               and ('antenatal' not in x.vaccines_received
        #               or (len(x.vaccine_times) > 0 and t - x.vaccine_times[-1] > (280 / (364.0 / self.t_per_year))))]

        return candidates
