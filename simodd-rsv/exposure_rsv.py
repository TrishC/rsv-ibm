from math import sin, pi
import numpy as np

from disease.general.exposure_base import Exposure
from population.pop_info import housemates
from population.utils import sample


class ExposureRSV(Exposure):
    def __init__(self, p):
        """
        Initialise a new Exposure object -- calculates FOI

        :param p: parameter dictionary
        """
        super(ExposureRSV, self).__init__(p)

        # transmission coefficients
        self.q = p['q'] * 364.0 / p['t_per_year']
        self.q_h = p['q_h'] * 364.0 / p['t_per_year']

        # household transmission:
        # alpha = 0 : density dependent household mixing
        # alpha = 1 : frequency dependent household mixing
        self.alpha = p.get('alpha', 1.0)

        # seasonal forcing: amplitude
        self.sf_amp = p.get('sf_amp', 0.0)
        # seasonal forcing: pre-compute 2*pi
        self.two_pi = 2 * pi / p['t_per_year']

        # susceptibility reduction due to maternal immunity
        self.susc_factor_vacc = p['susc_factor_vacc']
        self.susc_factor_inf = p['susc_factor_inf']

        # infectiousness scaling if over 10 years
        self.reduced_infect = p['reduced_infect']


    def calc_foi_fast(self, t, ind, P, comm_precomp, rng):
        """
        Calculates force of infection acting on an individual based on prevalence
        of infection in household and community

        :param t: current timestep
        :param ind: current individual
        :param P: population
        :param comm_precomp: pre-computed age-specific community FOI
        :param rng: random number generate
        :return: FOI acting on ind
        """

        # list of hh_members (NB: does NOT include ind, so no need to subtract 1 from size)
        hh_members = housemates(P, ind)

        # tuple of infectious hh members (I_H) and their age
        hh_I = [(x.ID, x.age) for x in hh_members if x.state.infectious]

        # array of infectious contribution from infectious individuals
        if hh_I:
            hh_infectiousness = [1.0] * len(hh_I) #np.ones((1, len(hh_I)))
            for idx, val in enumerate(hh_I):
                if val[1] >= 10:
                    hh_infectiousness[idx] = self.reduced_infect
        else:
            hh_infectiousness = [0] #np.zeros(1)

        hh_I_contrib = np.sum(hh_infectiousness) if hh_I else 0

        # effective proportion of household members infectious (infectiousness * I_H / (N_H - 1)^alpha)
        hh_I_prop = hh_I_contrib / pow(float(len(hh_members)), self.alpha) if hh_members else 0

        # calculate household contribution
        hh = self.q_h * hh_I_prop

        # calculate community contribution
        # (uses pre-computed values for \sum_j (\eta_{ij} I_j / N_j) )
        comm = self.q * comm_precomp['I'][ind.age]

        # combined exposure is sum of household and community
        combined = hh + comm

        # determine if household source and if so, age of source
        ind.hh_frac = hh / combined if combined > 0.0 else 0.0
        if ind.hh_frac > 0.0 and rng.random() < ind.hh_frac:
            sample_weights = [x / hh_I_contrib for x in hh_infectiousness]
            hh_infect_IDs = [x[0] for x in hh_I]
            ind.hh_source = 1
            ind.source = np.random.choice(hh_infect_IDs, p=sample_weights)
        else:
            ind.hh_source = 0
            ind.source = 0

        # compute seasonal forcing term
        sf = (1.0 + (self.sf_amp * sin(t * self.two_pi))) if self.sf_amp > 0.0 else 1.0

        # reduce force of infection depending on source of maternal immunity
        susc_factor = 1

        if ind.state.label == 'M_v':
            susc_factor = self.susc_factor_vacc
        elif ind.state.label == 'M_i':
            susc_factor=  self.susc_factor_inf

        return sf * susc_factor * combined
