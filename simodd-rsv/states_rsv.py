"""
Basic disease states.
"""

from math import exp

from disease.general.state_base import State, StateTimed


class SusceptibleTemp(StateTimed):
    """Susceptible state"""

    def __init__(self, order, duration, label='S_t', color='k'):
        super(SusceptibleTemp, self).__init__(order, duration, label, color)
        self.at_risk = True
        self.infectious = False

    def update(self, t, ind, states):
        if super(SusceptibleTemp, self).update(t, ind, states):
            ind.next_state = states['S']

    def test_exposure(self, states, ind, foi, rng):
        if rng.random() < 1.0 - exp(-foi):
            if 'E' in states:
                ind.next_state = states['E']
            else:
                ind.next_state = states['I']
            return "infection"
        else:
            return False



            ###############################################################################

