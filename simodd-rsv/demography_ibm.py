~# script to load the population formed by the demographic burn in and extract
# the age and household distribution

import os
import sys
import numpy as np

# set paths - users need to  update paths to match their system
repo_path = '~/path/to/repo'
sys.path.append(os.path.join(repo_path, '/simodd-pop'))
sys.path.append(os.path.join(repo_path, '/simodd-dis'))
sys.path.append(os.path.join(repo_path, '/simodd-rsv'))

from population.pop_io import *
from population.pop_info import hh_size, age_dist

# load population
pop_filename = 'population_checkpoints/burn_in_31328.pop'
P = load_pop(pop_filename)


# write out age distribution - users need to update paths to match their system
demo_P = age_dist(P.I, num_bins=101, max_age=101, norm=True)
csvP = zip(demo_P[0], demo_P[1])

with open('~/path/to/repo/burn_in_dist.csv', 'w') as f:
    writer = csv.writer(f, delimiter=',')
    writer.writerows(csvP)

# write out hh distribution - users need to update paths to match their system
by_size = dict([(x, []) for x in range(1, 11)])
for grp in P.groups['household'].values():
           by_size[min(10, len(grp))].extend(grp)

key_lst = []
len_lst = []
for key, value in by_size.items():
    key_lst.append(key)
    len_lst.append(value.__len__())

rows = zip(key_lst, len_lst)

with open('~/path/to/repo/hh_dist.csv', 'w') as f:
    writer = csv.writer(f, delimiter=',')
    writer.writerows(rows)












