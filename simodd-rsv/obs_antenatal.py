__author__ = 'ngeard'


## NB: currently hardcoded for 1 day time step

from disease.observers.obs_base import Observer
import tables as tb
import numpy as np


class AntenatalObserver(Observer):
    def __init__(self, h5file, start_time):
        self.start_time = start_time
        desc = {
            'infant_id': tb.UInt32Col(),
            'time_birth': tb.UInt32Col(),
            'mother_vacc': tb.UInt8Col(),
            'time_vacc': tb.Int32Col(),
            'mother_infect': tb.UInt8Col(),
            'time_infect': tb.Int32Col(),
            'birth_state': tb.StringCol(4),
            'birth_immunity': tb.UInt32Col()}
        super(AntenatalObserver, self).__init__(h5file, 'antenatal', desc, 'Antenatal Observer')

    def update(self, t, pop, **kwargs):
        if t < self.start_time:
            return

        # Get a list of all infants born during this time step.
        new_births = [x for x in pop.I_by_age[0].values() if x.age_days == 0]

        # Record information on newborns and their mothers.
        for cur_birth in new_births:
            self.row['infant_id'] = cur_birth.ID
            self.row['time_birth'] = t
            mother = cur_birth.parents[0]
            mother_ever_vaccinated = len(mother.vaccines_received) > 0
            mother_antenatal_last = mother_ever_vaccinated and \
                'antenatal' in mother.vaccines_received[-1] \
                if mother_ever_vaccinated else 0
            mother_vaccinated = mother_antenatal_last and (t - mother.vaccine_times[-1]) \
                < 280  # note hard coded for daily time step. may be able to avoid this using
            #  mothers age in days and mother's previous birth age (which is age at this birth)
            self.row['mother_vacc'] = mother_vaccinated
            self.row['time_vacc'] = mother.vaccine_times[-1] if mother_vaccinated else -1
            mother_infected = len(mother.infections) > 0
            self.row['mother_infect'] = mother_infected
            self.row['time_infect'] = mother.infections[-1] if mother_infected else -1
            self.row['birth_state'] = cur_birth.state.label if cur_birth.state.label \
                not in ['E', 'I'] else cur_birth.prev_state.label
            self.row['birth_immunity'] = cur_birth.counters.get('M_i', 0) + cur_birth.counters.get('M_v', 0)

            self.row.append()
            self.h5file.flush()

    def export_as_csv(self, filename):
        subset = []
        subset.append(self.data.col('infant_id'))
        subset.append(self.data.col('time_birth'))
        subset.append(self.data.col('birth_state'))
        subset.append(self.data.col('birth_immunity'))
        subset.append(self.data.col('mother_vacc'))
        subset.append(self.data.col('time_vacc'))
        subset.append(self.data.col('mother_infect'))
        subset.append(self.data.col('time_infect'))
        s_array = np.array(subset)
        s_array = np.transpose(s_array)
        np.savetxt(filename, s_array, delimiter=',', fmt='%d',
                   header='infant_id, time_birth, maternal_immunity, mat_imm_dur, mother_vacc, time_vacc, mother_infect, time_infect')

