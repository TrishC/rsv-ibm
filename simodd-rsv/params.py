__author__ = 'ngeard'

p = {
    # directories
    'resource_prefix': 'data/',
    'prefix': 'output/',

    # demographic model parameters
    'hh_composition': 'hh_comp_1910.dat',
    'age_distribution': 'age_struct_1921.dat',
    'fertility_parity_probs': 'fertility_age_parity_probs.dat',
    'fertility_age_probs': 'fertility_age_probs_1910-2014.dat',
    'death_rates_m': 'death_rates_1910-2014_male.dat',
    'death_rates_f': 'death_rates_1910-2014_female.dat',
    'leaving_prob_file': 'leaving_probs.dat',
    'couple_prob_file': 'couple_probs.dat',
    'divorce_prob_file': 'divorce_probs.dat',
    'growth_rate_file': 'growth_rates-4.dat',
    'imm_rate_file': 'imm_rates-zero.dat',
    'birth_num_file': 'birth_num.dat',

    'couple_prob': 0.08,
    'leaving_prob': 0.02,
    'divorce_prob': 0.01,
    'couple_age': 18,
    'couple_age_max': 60,
    'leaving_age': 18,
    'divorce_age': 18,
    'divorce_age_max': 60,
    'partner_age_diff': -2,
    'partner_age_sd': 2,
    'min_partner_age': 16,
    'birth_gap_mean': 270,
    'birth_gap_sd': 1,

    'pop_size': 31328, # 31328 yields population size of approx 100,000
    'growth_rate': 0.00,
    'imm_rate': 0.0,
    'immigration_prob': 0.0,

    # motherhood parameters
    'preg': True,
    'use_parity': False,
    'dyn_rates': False,
    'update_demog': True,
    'never_mother_prob': 0.13,

    # contact model parameters
    'cm_gauss': True,
    'cm_smooth': True,
    'sigma_2': 10.0,
    'epsilon': 0.8,
    'phi': 0.7,
    'cm_update_years': 5,

    # household transmission factor (1 == frequency dependent)
    'alpha': 1.0,

    # disease model parameters

    # durations in each disease compartment
    'infected_mean': 9,  # days
    'exposed_mean': 4,  # days
    'removed_mean': 230,  # days
    'vaccinated_mean': 230,  # days
    'maternal_mean': 90,  # days

    # reductions in susceptibility from maternal protection
    'susc_factor_vacc': 0.4,
    'susc_factor_inf' : 0.4,

    # reduction in infectiousness in 10 years and older (selected from sweeps)
    'reduced_infect': 0.2,

    # seasonal forcing amplitude
    'sf_amp': 0.397,

    # community transmission coefficient (selected from sweeps)
    'q': 0.015,
    # household transmission coefficient (selected from sweeps)
    'q_h': 2.4,

    # shape parameter for gamma distribution
    'k': 3,

    'external_exposure_rate': 5e-6,

    'random_seed': False, # set to False for reproducibility
    'seed': 1234,
    't_per_year': 364,  # model time step after demo burn-in
    'years': [0, 10],
    'burn_in': 101,
    'burn_in_t_per_year': 1,
    'epi_burn_in': 10,

    'halt': False,

    # vaccine parameters
    'ga_max': 90,  # max days gestational age
    'ga_min': 42,  # min days gestational age
    'vacc_cov': 0,

    # run parameters
    'num_runs': 1,
    'initial_cases': 5,
    'output_list': ['all'],
    'save_cp': True,
    'logging': False,
}

p['demo_burn'] = p['burn_in'] + p['epi_burn_in']



