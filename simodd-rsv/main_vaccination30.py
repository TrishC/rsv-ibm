"""
Setup for multiple runs (to aggregate stats over)
"""

import os
import sys
import socket

# set paths - users need to  update this to match their system
repo_path = '~/path/to/repo'
sys.path.append(os.path.join(repo_path, '/simodd-pop'))
sys.path.append(os.path.join(repo_path, '/simodd-dis'))
sys.path.append(os.path.join(repo_path, '/simodd-rsv'))

resource_path = 'data'


from disease.general.contact_matrix import *
from disease.general.vaccine import cov_gen_interrupt
from disease.general.duration import DurationGenerator
from disease.general.run import go_single, output_single
from disease_rsv import DiseaseRSV
from exposure_rsv import ExposureRSV
from vaccine_antenatal import *
from disease.observers.obs_disease import DiseaseObserver
from disease.observers.obs_cases_table import CaseObserver
from disease.observers.obs_vaccine import VaccineObserver
from obs_antenatal import AntenatalObserver
from disease.experiments.param_combo import ParamComboIt
from random import Random

from params import p


class DiseaseLocal(DiseaseRSV):
    def __init__(self, p, cmatrix, rng, fname, mode='w'):
        super(DiseaseLocal, self).__init__(p, cmatrix, rng, fname, mode)

        # switch to new exposure function
        self.exposure = ExposureRSV(p)

        obs = [
            CaseObserver(self.h5file, p['t_per_year'], entry_state='In', store_source=False),
            DiseaseObserver(self.h5file, self.state_labels(), self.disease_states),
            AntenatalObserver(self.h5file,
                              start_time=(p['burn_in'] + p['epi_burn_in'] + vacc_start))
        ]

        # set up vaccines #####################################################

        # set coverage
        vacc_cov = [p['vacc_cov']]

        # local copy of vaccines
        vac = {}

        # waning dictionary stores the waning duration based on source of immunity
        waning_dict = {
            'inf': DurationGenerator(
                p['removed_mean'] / (364.0 / p['t_per_year']), p['k'], rng),
            'vacc': DurationGenerator(
                p['vaccinated_mean'] /(364.0 / p['t_per_year']), p['k'], rng)
            }


        # handle the maternal vaccine
        cov = vacc_cov
        vac = [VaccineAntenatal('antenatal', 0, 0,
             cov, t_per_year=p['t_per_year'],
             offset=p['burn_in'] + p['epi_burn_in'] + vacc_start,
             rng=rng, precond=None, fail_prob=0,
             v_state='V')]

        self.add_vaccines(*vac)
        self.add_observers(*obs)

        # add the vaccine observer; note that, in order for the count_new and birth_count records
        # to be valid, interval needs to be set to 1 (ie, record every time step); otherwise
        # the counts will only be recorded for the single time step in which data is recorded,
        # not (as should be the case) aggregated over all time steps since last recording.
#        self.add_observers(VaccineObserver(self.h5file, vac, interval=p['t_per_year']))  # record annually
        self.add_observers(VaccineObserver(self.h5file, vac, interval=1))  # record every time step



# - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - #
# - # - MAIN  - # - #
# - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - #

if __name__ == '__main__':

    # establish seeds to generate 100 baseline simulations
    seed_rng = Random(p['seed'])
    seeds = []

    for i in range(100):
        seeds.append(seed_rng.randint(0, 99999999))

    print(seeds)

    # setup parameters
    seed_num = int(sys.argv[2])

    p['seed'] = seeds[seed_num]
    p['resource_prefix'] = 'data'
    p['prefix'] = 'output/vaccination30/seed_%02d' % seed_num
    p['pop_prefix'] = 'population_checkpoints'
    p['epi_prefix'] = 'population_checkpoints'
    p['years'] = [0, 10]
    p['vacc_cov'] = 0.3
    vacc_start = 5  # 0 starts vaccination immediately after 5 years

    # overwrite existing output files if 'x' switch is passed
    p['overwrite'] = ('x' in sys.argv)
    p['overwrite_cp'] = ('x' in sys.argv)

    if 'fake' in sys.argv:
        p['fake'] = True

    p['save_cp'] = True

    print('p')

    cmatrix = ContactMatrix(age_classes=mossong_age_classes_lo, a_levels=mossong_a_levels_lo)

    if sys.argv[1] == 's':
        # run individual simulation
        go_single(p, DiseaseLocal, cmatrix, p['seed'], verbose=True)

    elif sys.argv[1] == 'so':
        # plot individual result
        d = DiseaseLocal(p, cmatrix, Random(p['seed']),
                         os.path.join(p['prefix'], 'disease.hd5'), mode='r')
        output_single(p, d)

