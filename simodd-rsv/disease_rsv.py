import numpy as np

from exposure_rsv import ExposureRSV
from disease.SEIR.states_SEIR import Infected, Susceptible, Exposed, Removed, RemovedTemp, Maternal
from states_rsv import SusceptibleTemp
from disease.general.disease_base import DiseaseBase
from disease.general.duration import DurationGenerator, DurationGeneratorExpo, DurationGeneratorFixed

__author__ = 'ngeard'


class DiseaseRSV(DiseaseBase):
    """
    SIR model with temporary immunity arising from infection, and
    from maternal antibodies or vaccination
    """

    def __init__(self, p, cmatrix, rng, fname, mode):
        super(DiseaseRSV, self).__init__(p, None, cmatrix, fname, mode)
        self.exposure = ExposureRSV(p)
        vac = RemovedTemp(6, DurationGenerator(
            p['vaccinated_mean'] / (364.0 / p['t_per_year']), p['k'], rng), label='V')
        rem = RemovedTemp(5, DurationGenerator(
            p['removed_mean'] / (364.0 / p['t_per_year']), p['k'], rng), label='R')
        inf = Infected(4, DurationGenerator(
            p['infected_mean'] / (364.0 / p['t_per_year']), p['k'], rng))
        expos = Exposed(3, DurationGenerator(
            p['exposed_mean'] / (364.0 / p['t_per_year']), p['k'], rng))
        sus = Susceptible(2)
        mat_v = SusceptibleTemp(1, DurationGenerator(
            p['maternal_mean'] / (364.0 / p['t_per_year']), p['k'], rng), label='M_v')
        mat_i = SusceptibleTemp(0, DurationGenerator(
            p['maternal_mean'] / (364.0 / p['t_per_year']), p['k'], rng), label='M_i')

        self.add_states(mat_v, mat_i, sus, expos, inf, rem, vac)

        # factor for scaling amount of mum's immunity to bub
        self.mum_bub_immunity_factor = float(p['maternal_mean']) / p['removed_mean']
        self.mum_bub_immunity_factor_vacc = float(p['maternal_mean'] /
                                                  (p['vaccinated_mean']
                                                   - (p['ga_max'] + p['ga_min']) / 2))
        # factor for scaling infectiousness if over 10 years when infected
        self.reduced_infect = p['reduced_infect']


    def bd_update(self, t, births, deaths, imms, rng):
        """
        Overrides disease_base.bd_update

        For newly-born individuals, initialise them in M rather than S depending
        on mother's disease state.

        NB: current implementation (because it calls super function first) will
        record in individual log that they entered S and then M on same timestep.
        If this becomes an issue, may need to rewrite more of super function...
        this is only an issue if using debug logging, won't affect model output
        """
        super(DiseaseRSV, self).bd_update(t, births, deaths, imms, rng)

        for ind in births:
            mother = ind.parents[0]
            # if mother currently has protection from vaccination
            if mother.state.label in ['V']:
                ind.next_state = self.states['M_v']
                self.tick(t, ind)
                # set infant's immunity duration to some fraction of mothers
                mother_immunity = mother.counters.get('V', 0)
                ind.counters['M_v'] = self.mum_bub_immunity_factor_vacc * mother_immunity
            # if mother currently has protection from immunity
            if mother.state.label in ['R']:
                ind.next_state = self.states['M_i']
                self.tick(t, ind)
                # set infant's immunity duration to some fraction of mothers
                mother_immunity = mother.counters.get('R', 0)
                ind.counters['M_i'] = self.mum_bub_immunity_factor * mother_immunity
            # store birth state
            ind.birth_state = ind.state.label

    def update(self, t, P, rng):
        """
        Update the disease state of the population.
        """

        self.check_vaccines(t, P, rng)
        cases = self.check_exposure(t, P, rng)
        introductions = self.external_exposure(t, P, rng)
        # print "(update)", introduction
        new_I = self.update_ind_states(t, P)

        self.update_observers(t, disease=self, pop=P,
             cases=cases['infection'],
             boosting=cases['boosting'],
             introductions=introductions,
             new_I=new_I, rng=rng)

        # if halting, halt if no exposed or infected individuals in population (eg for SIR)
        return self.halt and not sum([self.states[x].count for x in self.disease_states])

    def check_exposure(self, t, P, rng):
        """
        Check all at risk individuals for potential exposure to infection.
        """

        # we will return a list of exposures (broken into infectious cases and boosting)
        cases = dict(infection=[], boosting=[])

        # return empty lists if there is currently no force of infection acting
        if sum([self.states[label].count for label in self.infectious_states]) == 0:
            return cases

        # create a set of individuals currently at risk
        # split depending upon whether network or matrix is being used to calculate community exposure
        pop_at_risk = set()
        comm = {}
        if self.cnetwork:
            # population at risk consists of household members and network neighbours of
            # people currently in an infectious state
            for cur_inf_state in self.infectious_states:
                for cur_I in self.states[cur_inf_state].current:
                    pop_at_risk.update([x for x in P.housemates(P.I[cur_I]) if x.state.at_risk])
                    pop_at_risk.update([x for x in self.cnetwork.get_contacts(P, P.I[cur_I]) if x.state.at_risk])
        else:
            # population at risk consists of potentially everybody
            pop_at_risk = [x for x in P.I.values() if x.state.at_risk]
            # compute exposure from community:
            # comm is the force of infection arising from infection in the community
            # EC[i] is a vector containing the contact rates between an individual in age group i
            # and individuals in each age group j, weighted by the (approximate) number of people
            # in age group j (as community mixing is density dependent).
            # I_by_age is the number of infected individuals in each age group j
            # infectiousness is the contribution an individual of a certain age makes to the FOI
            # total force of infection for each age class i is equal to the products of
            # weighted contact rate and the number of infected individuals, summed over
            # each age class i.

            # generate array of age specific relative infectiousness
            infect_lt10 = np.ones([1, 10])
            infect_gt10 = np.ones([1, (self.cmatrix.EC.shape[1] - 10)]) * self.reduced_infect
            infectiousness = np.hstack((infect_lt10, infect_gt10))

            for label in self.infectious_states:
                if self.cmatrix:
                    comm[label] = np.array([np.sum(self.cmatrix.EC[i] * self.by_age[label] * infectiousness)\
                                            for i in range(101)])
                else:
                    # dummy line for non-age based mixing
                    comm[label] = np.array([float(self.states[label].count) / len(P.I) for _ in range(101)])

        # test exposure for each individual at risk
        for ind in pop_at_risk:
            # split depending upon whether network or matrix is being used to calculate community exposure
            if self.cnetwork:
                foi = self.exposure.calc_foi_fast(t, ind, P, self.cnetwork, rng)
            else:
                foi = self.exposure.calc_foi_fast(t, ind, P, comm, rng)
            exposure_type = ind.state.test_exposure(self.states, ind, foi, rng)
            #                    p, s = P.hh_parents_siblings(ind)
            #                    p_I = len([x for x in p if x.state.infectious])
            #                    s_I = len([x for x in s if x.state.infectious])
            if exposure_type == 'infection':
                ind.infections.append(t)
                cases['infection'].append(ind)
            elif exposure_type == 'boosting':
                cases['boosting'].append(ind)
        return cases




